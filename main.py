from kivy.app import App
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import *
from kivy.uix.textinput import TextInput
Builder.load_file("./main.kv")


class PantallaPrincipal(Screen):
    pass

class SegundaPantalla(Screen):
    valor_malo = None
    def checkbox_clicked(self,value,name):
        self.name =name
        if self.ids.dinamo_bueno.state == 'down':
            print("bueno")
        elif self.ids.dinamo_regular.state == 'down':
            print(self.valor_malo)
        elif self.ids.dinamo_malo.state == "down":
            self.valor_malo = self.ids.dinamo_malo.state
            print("Malo")
            return self.valor_malo
        
    
    
    

   
     
class TerceraPantalla(Screen):
    pass

class MyApp(App):

    def build(self):
        sm=ScreenManager(transition=WipeTransition())
        sm.add_widget(PantallaPrincipal(name='inicio'))
        sm.add_widget(SegundaPantalla(name='segundapantalla'))
        sm.add_widget(TerceraPantalla(name='tercerapantalla'))
        return sm


if __name__ == '__main__':
    MyApp().run()